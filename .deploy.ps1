#!/usr/bin/env pwsh
[CmdletBinding()]
param (
	# silent mode, only errors
	[Parameter()]
	[Alias('s')]
	[switch]
	$silent
)

Set-StrictMode -Version 'Latest'

$Script:ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop

$Script:total_errors = 0

function Copy-Write {
	[CmdletBinding()]
	param (
		# Path to source file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to source file.")]
		[Alias('s', 'src')]
		[ValidateNotNullOrEmpty()]
		[string]
		$srcfile,
		# Path to destination file
		[Parameter(
			Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to destination file.")]
		[Alias('d', 'dst')]
		[ValidateNotNullOrEmpty()]
		[string]
		$dstfile
	)

	New-Variable -Name r -Force -ErrorAction SilentlyContinue
	$r = Copy-ModifiedFile -src $srcfile -dst $dstfile

	if ($r.GetType().Name -eq 'ErrorRecord') {
		Write-ErrorObject $r | Write-StdErr
	} elseif ($r -eq $true) {
		if (! $silent) {
			Write-Host ("copy " + $srcfile + " -> " + $dstfile)
		}
	}

	return $r
}

function Copy-Handler {
	[CmdletBinding()]
	param (
		# Source path
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Source path.")]
		[Alias('s')]
		[ValidateNotNullOrEmpty()]
		[string]
		$src,
		# Destination path
		[Parameter(Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Destination path.")]
		[Alias('d')]
		[ValidateNotNullOrEmpty()]
		[string]
		$dst,
		# recurse mode
		[Parameter()]
		[Alias('r')]
		[switch]
		$recurse
	)

	$Result = @()

	$check_src = IsDirectory -path $src

	if ($check_src.GetType().Name -eq 'ErrorRecord') {
		Write-ErrorObject $check_src | Write-StdErr
		$Result += [PSObject]@{src = $src; dst = $dst; result = $null}
		return $Result
		break
	}

	if ($check_src) {
		if ($recurse) {
			# recurse directory
			Get-ChildItem -Recurse -Force -LiteralPath $src | Where-Object {! $_.PSIsContainer} | Where-Object {
				New-Variable -Name r -Force -ErrorAction SilentlyContinue
				$srcfile = $_.FullName
				$dstfile = $(Join-Path -Path $dst -ChildPath $srcfile.Substring($src.Length))
				$r = Copy-Write -src $srcfile -dst $dstfile
				if ($r.GetType().Name -eq 'ErrorRecord') {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $null}
				} elseif ($r -eq $true) {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $true}
				} else {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $false}
				}
			}
		} else {
			# directory
			Get-ChildItem -Force -LiteralPath $src | Where-Object {! $_.PSIsContainer} | Where-Object {
				New-Variable -Name r -Force -ErrorAction SilentlyContinue
				$srcfile = $_.FullName
				$dstfile = $(Join-Path -Path $dst -ChildPath $srcfile.Substring($src.Length))
				$r = Copy-Write -src $srcfile -dst $dstfile
				if ($r.GetType().Name -eq 'ErrorRecord') {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $null}
				} elseif ($r -eq $true) {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $true}
				} else {
					$Result += [PSObject]@{src = $srcfile; dst = $dstfile; result = $false}
				}
			}
		}
	} else {
		# file
		if ($dst.EndsWith([IO.Path]::DirectorySeparatorChar)) {
			$dst = Join-Path -Path $dst -ChildPath $(Split-Path -Leaf -Path $src)
		}

		New-Variable -Name r -Force -ErrorAction SilentlyContinue
		$r = Copy-Write -src $src -dst $dst
		if ($r.GetType().Name -eq 'ErrorRecord') {
			$Result += [PSObject]@{src = $src; dst = $dst; result = $null}
		} elseif ($r -eq $true) {
			$Result += [PSObject]@{src = $src; dst = $dst; result = $true}
		} else {
			$Result += [PSObject]@{src = $src; dst = $dst; result = $false}
		}
	}
	return $Result
}

function Invoke-Handler {
	[CmdletBinding()]
	param (
		# Path to the executable file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true)]
		[Alias("b")]
		[ValidateNotNullOrEmpty()]
		[string]
		$bin,
		# Arguments list
		[Parameter(
			Mandatory=$false,
			Position=1)]
		[Alias("a")]
		[string]
		$param
	)

	$commandline = $bin
	if (! [string]::IsNullOrEmpty($param)) {
		$commandline = $commandline + ' ' + $param
	}
	if (! $silent) {
		Write-Host $commandline
	}
	try {
		# https://docs.microsoft.com/ru-ru/dotnet/api/system.diagnostics.processstartinfo
		$ProcessStartInfo = New-Object System.Diagnostics.ProcessStartInfo
		$ProcessStartInfo.FileName = $bin
		$ProcessStartInfo.Arguments = $param
		$ProcessStartInfo.RedirectStandardError = $true
		$ProcessStartInfo.RedirectStandardOutput = $true
		$ProcessStartInfo.UseShellExecute = $false
		$ProcessStartInfo.CreateNoWindow = $true
		$ProcessStartInfo.WindowStyle = 'Hidden'
		# https://docs.microsoft.com/ru-ru/dotnet/api/system.diagnostics.process
		$Process = New-Object System.Diagnostics.Process
		$Process.StartInfo = $ProcessStartInfo
		$Process.Start() | Out-Null
		$stdout = $Process.StandardOutput.ReadToEnd()
		$stderr = $Process.StandardError.ReadToEnd()
		$Process.WaitForExit()
	}
	catch {
		# fail (execite process)
		foreach ($err in $Error) {
			$err | Write-StdErr
		}
		$Script:total_errors++
	}
	finally {
		$exitcode = $Process.ExitCode
		$Process.Close()
		# stdout
		if ((! $silent) -and ($stdout.Length -gt 0)) {
			Write-Host $stdout.Trim()
		}
		# exitcode
		if ([string]::IsNullOrEmpty($exitcode)) {
			# fail (not exist exitcode)
			if ($silent) {
				$commandline | Write-StdErr
			}
			"return not exist" | Write-StdErr
			$Script:total_errors++
		} elseif ($exitcode -ne 0) {
			# fail (bad exitcode)
			if ($silent) {
				$commandline | Write-StdErr
			}
			"return $exitcode" | Write-StdErr
			$Script:total_errors++
		}
		# stderr
		if ($stderr.Length -gt 0) {
			$stderr.Trim() | Write-StdErr
		}
	}
}

function Copy-ModifiedFile {
	[CmdletBinding()]
	param (
		# Path from source file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path from source file.")]
		[Alias("s", "src")]
		[ValidateNotNullOrEmpty()]
		[string]
		$srcfile,
		# Path to destination file
		[Parameter(
			Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to destination file.")]
		[Alias("d", "dst")]
		[ValidateNotNullOrEmpty()]
		[string]
		$dstfile
	)

	# $fileName = ''
	# $file = [System.io.File]::Open($fileName, 'Open', 'Read', 'None'); Write-Host "Press any key to continue ..."; $null = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown"); $file.Close()

	try {
		if (! $(Test-Path -PathType Leaf -LiteralPath $dstfile)) {
			if (! $(Test-Path -PathType Container -LiteralPath $(Split-Path -Parent -Path $dstfile))) {
				New-Item -Force -ItemType Directory -Path $(Split-Path -Parent -Path $dstfile) | Out-Null
			}
			Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
			return $true
		} else {
			if ($(Get-FileHash -LiteralPath $srcfile).Hash -ne $(Get-FileHash -LiteralPath $dstfile).Hash) {
				Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
				return $true
			}
		}
		return $false
	}
	catch {
		return $_
	}
}

function Write-ErrorObject {
<#
	.SYNOPSIS
		Return string from ErrorRecord object.
#>

	param ([PSObject] $InputObject)

	begin {
		$line = @()
	}

	process {
		$line += 'Error'

		if ($InputObject.InvocationInfo.MyCommand) {
			$line += $InputObject.InvocationInfo.MyCommand.ToString()
		}

		$line += ':'

		if ($InputObject.Exception.Message) {
			$line += $InputObject.Exception.Message.ToString().Trim()
		}

		if ($InputObject.InvocationInfo.ScriptLineNumber) {
			$line += 'line'
			$line += $InputObject.InvocationInfo.ScriptLineNumber.ToString()
		}
	}

	end {
		return ($line -join ' ')
	}
}

function Write-StdErr {
<#
	.SYNOPSIS
		Writes text to stderr when running in a regular console window,
		to the hosts error stream otherwise.

	.DESCRIPTION
		Writing to true stderr allows you to write a well-behaved CLI
		as a PS script that can be invoked from a batch file, for instance.

	Note that PS by default sends ALL its streams to *stdout* when invoked from cmd.exe.

	This function acts similarly to Write-Host in that it simply calls
	.ToString() on its input; to get the default output format, invoke
	it via a pipeline and precede with Out-String.
#>

	param ([PSObject] $InputObject)

	$outFunc = if ($Host.Name -eq 'ConsoleHost') {
		[Console]::Error.WriteLine
	} else {
		$Host.UI.WriteErrorLine
	}

	if ($InputObject) {
		[void] $outFunc.Invoke($InputObject.ToString())
	} else {
		[string[]] $lines = @()
		$Input | ForEach-Object { $lines += $_.ToString() }
		[void] $outFunc.Invoke($lines -join [Environment]::NewLine)
	}
}

function IsDirectory {
	[CmdletBinding()]
	param (
		[Parameter(
			Mandatory=$true,
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true)]
		[ValidateNotNullOrEmpty()]
		[string]
		$path
	)

	try {
		if ((Get-Item -Force -LiteralPath $path -ErrorAction Stop) -is [System.IO.DirectoryInfo]) {
			return $true
		} else {
			return $false
		}
	}
	catch {
		return $_
	}
}

# custom part
# sourcing .deploy.custom.ps1
if (Test-Path -PathType Leaf -LiteralPath $([IO.Path]::Combine($PSScriptRoot, '.deploy.custom.ps1'))) {
	try {
		. ([IO.Path]::Combine($PSScriptRoot, '.deploy.custom.ps1'))
	}
	catch {
		Write-StdErr 'sourcing .deploy.custom.ps1 fail'
		$total_errors++
	}
}

# final
if ($total_errors -eq 0) {
	exit 0
} else {
	exit 1
}
