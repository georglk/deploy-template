# Deploy template

## File `.gitlab-ci.yml`

https://docs.gitlab.com/ee/ci/yaml/

Необходимо переименовать файл `.gitlab-ci.yml.disable` как `.gitlab-ci.yml`.

При необходимости внести изменения.

Например, указать `tags` для выбора конкретного Gitlab Runner из списка всех, доступных для проекта.

Вызов в `deploy:script` без явного указания `command-line shell` предполагает что в Gitlab Runner в качестве `shell` указан `pwsh` или `powershell`.

Подробнее о Shell executor https://docs.gitlab.com/runner/shells/

## File `.deploy.custom.ps1`

При наличие файла `.deploy.custom.ps1` в корне репозитория, он будет вызван из [`.deploy.ps1`](.deploy.ps1).

Примеры файлов
+ [.deploy.custom.ps1.linux-example](.deploy.custom.ps1.linux-example)
+ [.deploy.custom.ps1.win-example](.deploy.custom.ps1.win-example)
